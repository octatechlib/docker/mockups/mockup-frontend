FROM node:18.14.2-bullseye

WORKDIR /app

COPY . .

RUN apt-get update
RUN apt install xdg-utils --fix-missing -y

RUN chmod +x ./coverage.sh
RUN npm install

EXPOSE 5173
EXPOSE 51204
EXPOSE 51206